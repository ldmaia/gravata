package com.example.exemploviewpager;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ComponentesAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<Componentes> lista;

	public ComponentesAdapter(Context context, ArrayList<Componentes> lista){
		this.context = context;
		this.lista = lista;
	}
	
	@Override
	public int getCount() {

        return lista.size();
	}

	@Override
	public Object getItem(int position) {

        return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

        Componentes componentes = lista.get(position);
		View layout;
		
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			layout = inflater.inflate(R.layout.componentes, null);
		}
		else{
			layout = convertView;
		}
		
		
		TextView modelo = (TextView) layout.findViewById(R.id.t1);
		modelo.setText(componentes.getModelo());
		
		TextView marca = (TextView) layout.findViewById(R.id.t2);
		marca.setText(componentes.getMarca());
		
		ImageView iv = (ImageView) layout.findViewById(R.id.iv);
		iv.setImageResource(componentes.getCarroImagem(position));
		
		if(position % 2 == 0){
			layout.setBackgroundColor(Color.WHITE);
		}
		
		return layout;
	}



}
